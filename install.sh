#!/bin/bash
# -*- ENCODING: UTF-8 -*-

sudo dnf remove libreoffice-* totem gnome-boxes gnome-tour simple-scan gnome-contacts cheese gnome-maps gnome-photos gnome-connections rhythmbox -y
sudo dnf group remove "LibreOffice" -y

sudo dnf check-update || sudo dnf upgrade -y

# rpmfusion
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# code
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

sudo dnf upgrade -y
sudo dnf install dnf-plugins-core

# Codecs
sudo dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install -y lame\* --exclude=lame-devel
sudo dnf group upgrade --with-optional Multimedia -y

# Paquetes
sudo dnf install -y zsh kitty alacritty yacreader picom stow zathura zathura-pdf-mupdf zathura-cb neovim mpv htop neofetch golang bspwm sxhkd dmenu rofi lxappearance
sudo dnf install -y polybar feh code util-linux-user telegram-desktop llvm clang-tools-extra ripgrep jq

# Grupos
sudo dnf group install "C Development Tools and Libraries" -y
sudo dnf group install "Development Tools" -y

# Docker
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install -y docker-ce docker-ce-cli containerd.io
sudo dnf install -y moby-engine docker-compose docker-compose-plugin
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker

# flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# plugins 
sudo dnf install gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-good-extras gstreamer1-plugins-ugly unrar p7zip p7zip-plugins gstreamer1-plugin-openh264 mozilla-openh264 openh264 webp-pixbuf-loader gstreamer1-plugins-bad-free-fluidsynth gstreamer1-plugins-bad-free-wildmidi gstreamer1-svt-av1 libopenraw-pixbuf-loader dav1d file-roller -y

# nvidia
sudo dnf install akmod-nvidia -y

# video
sudo dnf install x264 h264enc x265 svt-av1 rav1e -y

# dofiles
git clone https://gitlab.com/kokegudiel2/dotfiles.git ~/.dotfiles

cd ~/.dotfiles
stow *

# chsh
chsh

# Extras
git clone https://github.com/jenv/jenv.git ~/.jenv
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
go install golang.org/x/tools/gopls@latest
sudo wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -O /usr/local/bin/yt-dlp
sudo chmod a+rx /usr/local/bin/yt-dlp

# cleanall
sudo dnf clean all
sudo dnf autoremove -y

# Fin
clear
echo "Pos ya estuvo"
